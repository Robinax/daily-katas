//getLength([1, [2, 3]]) ➞ 3

 let arrayone = [1, [2, 3]];

//getLength([1, [2, [3, 4]]]) ➞ 4

 let arraytwo = [1, [2, [3, 4]]];

//getLength([1, [2, [3, [4, [5, 6]]]]]) ➞ 6

 let arraythree = [1, [2, [3, [4, [5, 6]]]]];

//getLength([1, [2], 1, [2], 1]) ➞ 5

let arrayfour = [1, [2], 1, [2], 1];

let arrayfive = []
/*
Array.prototype.getLength = function() {
    let sum = 0;
    for (let i = 0; i < this.length; i ++) {
      sum += this[i].length ? this[i].getLength() : 1;
    }
    return sum;
  };


  console.log(arrayone.getLength())
  console.log(arraytwo.getLength())
  console.log(arraythree.getLength())
  console.log(arrayfour.getLength())
  console.log(arrayfive.getLength())

  const getLength = (arr) => {
    let a = arr.flat(Infinity);
    let b;
    if (arr.length = 0) {
      b = 0;
    } else b = a.length;
    return b;
  };
  
  console.log(getLength([1, [2, 3]]));*/

  console.log("Flat: " + arrayone.flat(Infinity).length)
  console.log("Flat: " + arraythree.flat(Infinity).length)
  console.log("Flat: " + arrayfour.flat(Infinity).length)
  console.log("Flat: " + arrayfive.flat(Infinity).length)